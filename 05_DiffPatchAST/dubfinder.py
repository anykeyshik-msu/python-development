from argparse import ArgumentParser
import ast
from difflib import SequenceMatcher
from inspect import getmembers, getsource, isclass, isfunction
from importlib import import_module
from textwrap import dedent
from prettytable import PrettyTable


MIN_DUB_RATIO = 0.95


def parse_object(obj, prefix=''):
    parses: dict[str, str] = {}
    for name, member in getmembers(obj):
        if isclass(member):
            if not name.startswith('__'):
                parses |= parse_object(member, prefix=f'{prefix}.{name}')
        elif isfunction(member):
            src = getsource(member)
            tree = ast.parse(dedent(src))
            for node in ast.walk(tree):
                for attr in ['name', 'id', 'arg', 'attr']:
                    if hasattr(node, attr):
                        setattr(node, attr, '_')

            parse = ast.unparse(tree)
            parses[f'{prefix}.{name}'] = parse

    return parses


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('modules', nargs='+', type=str,
                        help='Module names to inspect')

    args = parser.parse_args()
    out_table = PrettyTable(args.modules)

    func2parse = {}
    for module_name in args.modules:
        module = import_module(module_name)
        func2parse |= parse_object(module, prefix=module_name)

    funcs = sorted(func2parse.keys())
    for idx, first_func in enumerate(funcs):
        for second_func in funcs[idx + 1:]:
            first_parse = func2parse[first_func]
            second_parse = func2parse[second_func]

            ratio = SequenceMatcher(None, first_parse, second_parse).ratio()
            if ratio > MIN_DUB_RATIO:
                out_table.add_row([first_func, second_func])

    print(out_table)
