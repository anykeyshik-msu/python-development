# Взаимодействие на основе патчей. Исследование кода

### Текст задания
1. Написать программу `dubfinder.py`, которая исследует модули на наличие похожих функций или методов с учётом переименования объектов, наличия произвольных комментариев и изменений в форматировании.
    * Параметры командной строки: `dubfinder.py модуль1 необязательный модуль2 ...`
    * Программа выводит пары функций, исходный текст которых «очень похож» (см. ниже критерии)
    * Вывод: для `dubfinder.py spiral` ([spiral.py](https://uneex.org/LecturesCMC/PythonDevelopment2022/05_DiffPatchAST?action=AttachFile&do=view&target=spiral.py))
        ```
        $ python3 dubfinder.py spiral        
        spiral.Spiral.__add__ : spiral.Spiral.__sub__
        spiral.Spiral.__mul__ : spiral.Spiral.__rmul__
        ```
    * Вывод: для `dubfinder.py spiral spiral2` ([sprial.py](https://uneex.org/LecturesCMC/PythonDevelopment2022/05_DiffPatchAST?action=AttachFile&do=view&target=spiral.py) и [spiral2.py](https://uneex.org/LecturesCMC/PythonDevelopment2022/05_DiffPatchAST?action=AttachFile&do=view&target=spiral2.py))
        ```
        spiral.Spiral.__add__ spiral2.Spiral.__add__
        spiral.Spiral.__init__ spiral2.Spiral.__init__
        spiral.Spiral.__iter__ spiral2.Spiral.__iter__
        spiral.Spiral.__len__ spiral2.Spiral.__len__
        spiral.Spiral.__mul__ spiral.Spiral.__rmul__
        spiral.Spiral.__rmul__ spiral2.Spiral.__mul__
        spiral.Spiral.__str__ spiral2.Spiral.__str__
        spiral.Spiral.__sub__ spiral2.Spiral.__sub__
        spiral.Spiral._square spiral2.Spiral._show
        spiral.main spiral2.master
        spiral2.Spiral.__add__ spiral2.Spiral.__sub__
        spiral2.Spiral.__mul__ spiral2.Spiral.__rmul__
        ```
    * Требования по выводу __не вполне чёткие__  — он должен быть упорядочен лексикографически, но что делать, если «одинаковых» функций более двух, не регламентировано
        * Полное совпадение `__mul__` и `__rmul__` в одном модуле 
        * Частичное совпадение `__add__` и `__sub__` в одном модуле 
        * Полное совпадение соответствующих функций и методов в обоих модулях
    * Допущения относительно проверяемых модулей (для простоты): 
        * Не используются docstring-и и аннотации
        * Не используются подмодули
        * Не используются инструкции вида `from модуль import что-то`
2. Возможный вариант реализации (я сделал так, но теперь мне кажется, что он более сложный):
    1. Модули разрешено `import`-тить (с помощью [importlib](https://docs.python.org/3/library/importlib))
    2. Затем рекурсивно просмотрим их с помощью [inspect.getmembers()](https://docs.python.org/3/library/inspect.html#inspect.getmembers) и составим список определённых в них функций
        * Разрешено игнорировать __классы__ , имя которых начинается с `__` (например, не стоит рекурсивно заходить в `что_то.__class__`)
    3. Для каждой функции с помощью [inspect.getsource()](https://docs.python.org/3/library/inspect.html#inspect.getsource) получаем исходный текст.
    4. С помощью [ast.parse()](https://docs.python.org/3/library/ast.html#ast.parse) превращаем этот текст в дерево разбора (для методов надо предварительно применить [textwrap.dedent()](https://docs.python.org/3/library/textwrap.html#textwrap.dedent))
    5. Заменить в дереве все __идентификаторы__ на один (например, на `_`)
        * Я просто прошёлся по [ast.walk()](https://docs.python.org/3/library/ast.html#ast.walk) и подменил все атрибуты `name`, `id`, `arg`, `attr` у тех узлов, у которых они были
    6. Собрать обратно препарированный текст с помощью [ast.unparse()](https://docs.python.org/3/library/ast.html#ast.unparse)
        * Разумеется, комментарии при этом исчезают, а вместо имён везде стоит `_`
    7. Составим словарик вида `{найденная_функция: препарат}`
    8. Сравним все препараты друг с другом (это n²/2, увы) и для каждой функции подберём наиболее «близкую» с помощью [difflib.SequenceMatcher.ratio()](https://docs.python.org/3/library/difflib.html#difflib.SequenceMatcher.ratio)
    9. Если это `ratio()` > 0.95 — пара считается «похожей» 
3. Есть и другой вариант решения: вместо `importlib` и `inspect` использовать тот же `ast`, и вручную обходить дерево. Возможно, он проще.
4. В отчётном репозитории с Д/З создать подкаталог `05_DiffPatchAST` и поместить туда решение
