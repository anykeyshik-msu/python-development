from . import bullscows, gameplay
import sys
import urllib.request

def main():
    def ask(prompt: str, valid: list[str] = None) -> str:
        if valid is None:
         print(prompt)
         return input()

        while True:
         print(prompt)
         w = input()
         if w in valid:
                return w

    def inform(format_string: str, bulls: int, cows: int) -> None:
        print(format_string.format(bulls, cows))

    dict_path = sys.argv[1]
    try:
        f = open(dict_path, "r", encoding='utf8')
        words = f.read().split()
    except IOError:
        f = urllib.request.urlopen(dict_path)
        words = f.read().decode('utf-8').split()

    words_len = 5
    if len(sys.argv) > 2:
        words_len = int(sys.argv[2])

    words = [w for w in words if len(w) == words_len]

    print("Number of tries: ", gameplay(ask, inform, words))

if __name__ == '__main__':
    sys.exit(main())
