from textdistance import hamming, bag
from random import choice


def bullscows(guess: str, secret: str) -> (int, int):
    ham_diff = hamming.similarity(guess, secret)
    bag_diff = bag.similarity(guess, secret)

    return (ham_diff, bag_diff - ham_diff)


def gameplay(ask: callable, inform: callable, words: list[str]) -> int:
    secret = choice(words)
    tries = 0

    while True:
        guess = ask("Please enter your word: ", words)

        bulls, cows = bullscows(guess, secret)
        inform("Bulls: {}, Cows: {}", bulls, cows)

        count += 1

        if bulls == len(secret):
            return count
