# -*- coding: utf-8 -*-

#   Copyright (c) 2020.
#
#   Created by AnyKeyShik Rarity
#
#   Telegram: @AnyKeyShik
#   GitHub: https://github.com/AnyKeyShik
#   E-mail: nikitag594@gmail.com

from tempfile import mkdtemp
from argparse import ArgumentParser

import subprocess
import os
import venv
import shutil


def main():
    parser = ArgumentParser(description='Figdate wrapper')
    parser.add_argument('--format', type=str, default='%Y %d %b, %A')
    parser.add_argument('--font', type=str, default='graceful')
    args = parser.parse_args()

    tmpdir = mkdtemp()
    venvdir = venv.create(tmpdir, with_pip=True)

    pip_path = os.path.join(tmpdir, "bin", "pip")
    subprocess.run([pip_path, "install", "pyfiglet"])

    python_path = os.path.join(tmpdir, "bin", "python3")
    subprocess.run([
        python_path, "-m", "figdate",
        '--format', args.format, '--font', args.font
    ])

    shutil.rmtree(tmpdir)


if __name__ == "__main__":
    main()
