# -*- coding: utf-8 -*-

#   Copyright (c) 2022.
#
#   Created by AnyKeyShik Rarity
#
#   Telegram: @AnyKeyShik
#   GitHub: https://github.com/AnyKeyShik
#   E-mail: nikitag594@gmail.com

from datetime import datetime
from pyfiglet import Figlet


def date(format="%Y %d %b, %A", font="graceful") -> str:
    """
    Function returns figleted current date and time

    :param format: format for time
    :param font: font for render

    :return: figleted date and time
    :rtype: str
    """

    fig = Figlet(font=font)
    current_date = datetime.now()

    return fig.renderText(current_date.strftime(format))
