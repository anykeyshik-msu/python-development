# -*- coding: utf-8 -*-

#   Copyright (c) 2022.
#
#   Created by AnyKeyShik Rarity
#
#   Telegram: @AnyKeyShik
#   GitHub: https://github.com/AnyKeyShik
#   E-mail: nikitag594@gmail.com

from figdate import date
from sys import argv
from argparse import ArgumentParser

import locale


def main():
    parser = ArgumentParser(description='Render date and time in ASCII graphic')
    parser.add_argument('--format', type=str, default='%Y %d %b, %A')
    parser.add_argument('--font', type=str, default='graceful')
    args = parser.parse_args()

    loc = locale.getlocale()
    locale.setlocale(locale.LC_ALL, ('ru_RU', 'UTF-8'))
    print(date(args.format, args.font))
    locale.setlocale(locale.LC_ALL, loc)


if __name__ == "__main__":
    main()
