# Публичный репозиторий. Сторонние модули из pypi

### Текст задания
1. Написать [пакет](https://docs.python.org/3/tutorial/modules.html#packages) `figdate`, который предоставляет всего одну функцию — `date(формат, шрифт)`. Функция возвращает строку — текущую дату в виде [pyfiglet](https://github.com/pwaller/pyfiglet)
    * Формат совпадает с форматом [time.strftime()](https://docs.python.org/3/library/time.html#time.strftime) (является им ☺), по умолчанию — `"%Y %d %b, %A"`
    * Шрифт — строка, обозначающая шрифт в модуле [pyfiglet](https://github.com/pwaller/pyfiglet), по умолчанию — `"graceful"` (в нём есть кириллица)
    * Пакет также должен поддерживать запуск вида `python3 -m figdate` без параметров, с одним параметром (форматом) или двумя (форматом и шрифтом), вызывать функцию с этими параметрами и выводить результат
            * В этом случае следует [выставлять русскую локаль](https://docs.python.org/3/library/locale.html#background-details-hints-tips-and-caveats)
    * Пакет пишется в расчёте на то, что `figdate` будет доступен
2. Написать программу-обвязку `figdate_wrapper.py`, которая:
    * Создаёт чистое [venv](https://docs.python.org/3/library/venv.html#venv.create)-окружение во [временном каталоге](https://docs.python.org/3/library/tempfile.html#tempfile.mkdtemp), с pip-ом (параметр `with_pip`)
        * Если у вас проблемы с развёртыванием pip и setuptools, вам, гм…, повезло и вы получите ещё больше опыта! [Вот здесь](https://docs.python.org/3/library/venv.html#an-example-of-extending-envbuilder) есть про то, как и откуда их скачивать автоматом
    * Ставит с помощью pip-а в это окружение `pyfiglet`
        * **NB!** Pip принципиально не предоставляет никакого публичного API — это только сценарий. Так что придётся с помощью [subprocess.run()](https://docs.python.org/3/library/subprocess.html#subprocess.run) запустить что-то вроде `/временное/env/окружение/bin/pip install pyfiglet.`
    * Запускает `python3 -m figdate переданные параметры`
    * [Удаляет временный venv-подкаталог](https://docs.python.org/3/library/shutil.html#shutil.rmtree)
3. Таким образом даже в отсутствие пакета `pyfiglet` вся эта конструкция должна работать так:
    ```
    $ python3 figdate_wrapper.py
     ____   __  ____  ____     __   ___    ____  ____  ____      
    (___ \ /  \(___ \(___ \   /  \ / __)  / __ \(  __)(  _ \ _   
    / __/(  0  )/ __/ / __/  (_/ /(  _ \  \_  _/  ) _)  ) _ (( )  
    (____) \__/(____)(____)   (__) \___/   (__) (____)(____/(/   
         ___  ____  ____  ____   __  
        / __)(  _ \(  __ )/ __ \ / _\ 
        ( (__  ) __/  ) _) \_  _//    \
         \___)(__)  (____)(_)(_)\_/\_/
    ```

    * Причём если `pyfiglet`-а не было, то он и не появляется (сгинет вместе с временным `venv`-окружением)
4. Содзать в репозитории с домашним заданием подкаталог `02_PushPip` (имя совпадает с URL этой страницы), и положить туда
    * (как минимум, одним отдельным коммитом) каталог `figdate`
    * (как минимум, одним отдельным коммитом) обвязку `figdate_wrapper.py`
    * Ничего другого в этом подкаталоге (в частности, `__pycache__`) не должно быть (используем [.gitignore](https://git-scm.com/docs/gitignore)) 
