# Сложное слияние и метки. Командная строка

### Текст задания
1. Почитать про [shlex](https://docs.python.org/3/library/shlex) и [cmd](https://docs.python.org/3/library/cmd)
2. Поисследовать [pynames](https://pypi.python.org/pypi/pynames) (пример на странице проекта, но скорее всего придётся читать и исходники)
3. Написать интерпретатор командной строки с достраиванием (completion), который позволяет
    * Генерировать имя
        * произвольногй расы с произвольным подклассом (подклассы есть, например, в `generators.elven` и в `generators.iron_kingdoms`; если подкласс не указан, выбирается первый)
        * с необязательным указанием пола (male, female, по умолчанию — male, как в API)
    * Задавать язык для всех генераторов
        * В API он передаётся вторым параметром в генератор
        * Значение по умолчанию — `native`
        * Если генератор не поддерживает установленный язык, используется `native`
    * Получать информацию о конкретном генераторе (количество имён обоих полов и список языков) 
    * Формат команд:
        * `language RU`
        * `generate mongolian female`
            * `Квутух`
        * `generate iron_kingdoms Gobber`
            * `Vog-Kat-Ad-gamun`
        * `generate elven Warhammer`
            * `Торрион`
        * `info scandinavian male`
            * `1239`
        * `info scandinavian`
            * `1599`
        * `info scandinavian language`
            * `en ru`
    * Обратите внимание на то, что имя расы совпадает с именем модуля, а имя подкласса — с префиксом имени генератора. В примере выше `generate iron_kingdoms Gobber` выглядит так, потому что существует генератор `pynames.generators.iron_kingdoms.GobberFullnameGenerator()`
        * ⇒ достраивание можно сделать на несложном исследовании пространств имён модулей
        * например, выбрать оттуда классы вида `Что-то-тамКак-тоGenerator`, и выкинуть из этого списка базовые классы, которые автор зачем-то положил прямо рядом с настоящими генераторами
        * это же исследование даст нам список рас — модулей, в которых есть такие классы
4. Разработку вести согласно дисциплине оформления коммитов в подкаталоге `04_MergetoolCommandline` отчётного репозитория по Д/З
