import shlex
import cmd
import pynames
import collections


class Interpreter(cmd.Cmd):
    def __init__(self):
        super().__init__()
        self.lang = pynames.LANGUAGE.NATIVE
        self.prompt = '> '
        self.races_gens = collections.defaultdict(dict)

        for subclass in pynames.utils.get_all_generators():
            self.races_gens[subclass.__module__.split('.')[2]][subclass.__name__] = subclass


    def do_language(self, arg):
        args = shlex.split(arg)
        language = args[0].lower()

        if language == 'ru':
            self.lang = pynames.LANGUAGE.RU
        elif language == 'en':
            self.lang = pynames.LANGUAGE.EN
        elif language == 'native':
            self.lang = pynames.LANGUAGE.NATIVE


    def do_info(self, arg):
        args = shlex.split(arg)
        race_name = args[0].lower()

        gens_dict = self.races_gens[race_name]
        generators = [s for s in gens_dict if s.endswith('NamesGenerator') or s.endswith('FullnameGenerator')]

        for gen_name in generators:
            gen_class = gens_dict[gen_name]()

            match args:
                case [_, 'language']:
                    print(f"Class {gen_name} has {', '.join(gen_class.languages)} languages")
                case [_, _]:
                    match args[-1].lower():
                        case 'male':
                            print(f'Class {gen_name} has {gen_class.get_names_number(pynames.GENDER.MALE)} male names')
                        case 'female':
                            print(f'Class {gen_name} has {gen_class.get_names_number(pynames.GENDER.FEMALE)} female names')
                case [_]:
                    print(f'Class {gen_name} has {gen_class.get_names_number()} names')


    def do_generate(self, arg):
        args = shlex.split(arg)
        race_name = args[0].lower()

        gens_dict = self.races_gens[race_name]

        match args:
            case ['iron_kingdoms', _]:
                gen_name = args[1] + 'FullnameGenerator'
                gen_class = gens_dict[gen_name]()

                if self.lang in gen_class.languages:
                    print(gen_class.get_name_simple(language=self.lang))
                else:
                    print(gen_class.get_name_simple(language=pynames.LANGUAGE.NATIVE))
            case ['elven', _]:
                gen_name = args[1] + 'NamesGenerator'
                gen_class = gens_dict[gen_name]()

                if self.lang in gen_class.languages:
                    print(gen_class.get_name_simple(language=self.lang))
                else:
                    print(gen_class.get_name_simple(language=pynames.LANGUAGE.NATIVE))
            case [_, _]:
                gen_name = [s for s in gens_dict if s.endswith('NamesGenerator')][0]
                gen_class = gens_dict[gen_name]()

                match args[1]:
                    case 'male':
                        if self.lang in gen_class.languages:
                            print(gen_class.get_name_simple(gender=pynames.GENDER.MALE, language=self.lang))
                        else:
                            print(gen_class.get_name_simple(language=pynames.LANGUAGE.NATIVE))
                    case 'female':
                        if self.lang in gen_class.languages:
                            print(gen_class.get_name_simple(gender=pynames.GENDER.FEMALE, language=self.lang))
                        else:
                            print(gen_class.get_name_simple(language=pynames.LANGUAGE.NATIVE))



if __name__ == "__main__":
    intr = Interpreter()
    intr.cmdloop()
